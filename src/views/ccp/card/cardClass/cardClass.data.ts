import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';
import { DictEnum } from '@/enums/dictEnum';
import { uploadApi } from '@/api/upload';
import { Option } from '@/store/modules/dict';
import { listUserByDeptId } from '@/api/system/user';

export const formSchemas: FormSchema[] = [
  {
    label: '卡类名称',
    field: 'cardClassName',
    component: 'Input',
  },
  {
    label: '来源类型',
    field: 'sourceType',
    component: 'Select',
    componentProps: {
      options: getDictOptions(DictEnum.NORMAL_DISABLE),
    },
  },
  {
    label: '具体来源',
    field: 'sourceTypeDetail',
    component: 'Input',
  },
  {
    label: '负责人',
    field: 'director',
    component: 'Input',
  },
];

const { renderDict, renderTooltip } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '卡类名称',
    dataIndex: 'cardClassName',
    width: 240,
  },
  {
    title: '卡面图片',
    dataIndex: 'cover',
    width: 80,
    ellipsis: true,
    customRender({ value }) {
      return renderTooltip(value);
    },
  },
  {
    title: '主题颜色',
    dataIndex: 'subjectColor',
    width: 80,
  },
  {
    title: '来源类型',
    dataIndex: 'sourceType',
    width: 80,
    customRender({ value }) {
      return renderDict(value, 'ccp_source_type');
    },
  },
  {
    title: '具体来源',
    width: 80,
    dataIndex: 'sourceTypeDetail',
  },
  {
    title: '负责人',
    dataIndex: 'director',
  },
  {
    title: '权益状态',
    width: 80,
    dataIndex: 'equityStatus',
    customRender({ value }) {
      return renderDict(value, 'ccp_yes_no');
    },
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: '基础信息',
    field: 'baseInfo',
    component: 'Divider',
    componentProps: {
      orientation: 'left',
      plain: false,
    },
  },
  {
    label: 'ID',
    field: 'id',
    required: true,
    component: 'Input',
    show: false,
  },
  {
    label: '卡类名称',
    field: 'cardClassName',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '负责人',
    field: 'director',
    required: true,
    component: 'Select',
    componentProps: {
      // 选中了就只能修改 不能重置为无负责人
      allowClear: false,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '来源类型',
    field: 'sourceType',
    required: true,
    component: 'Select',
    componentProps({ formActionType }) {
      return {
        options: getDictOptions('ccp_source_type'),
        // 切换后清空校验结果
        onChange: async (e) => {
          let options: Option[] = [];
          if (e == 1) {
            options = getDictOptions('ccp_yes_no');
          } else {
            options = await initUsers();
          }
          await formActionType.updateSchema({
            field: 'sourceTypeDetail',
            componentProps: {
              options,
            },
          });
          if (formActionType) {
            await formActionType.clearValidate();
          }
        },
      };
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '具体来源',
    field: 'sourceTypeDetail',
    required: true,
    component: 'Select',
    colProps: {
      span: 12,
    },
  },
  {
    label: '主题颜色',
    field: 'subjectColor',
    required: true,
    slot: 'colorPicker',
    colProps: {
      span: 12,
    },
  },
  {
    label: '卡面图片',
    field: 'cover',
    required: true,
    component: 'ImageUpload',
    componentProps: {
      api: uploadApi,
      maxNumber: 1,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '是否去重',
    field: 'ifRepeat',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '强制激活',
    field: 'forceActivate',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.ifRepeat !== '0';
    },
  },
  {
    label: '校验绑定卡号',
    field: 'verifyBindCardNo',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '分区信息',
    field: 'partInfo',
    component: 'Divider',
    componentProps: {
      orientation: 'left',
      plain: false,
    },
  },
  {
    field: 'partitionInfo',
    required: true,
    slot: 'partitionInfo',
  },
  {
    label: '零售商信息表ID',
    field: 'retailerId',
    required: true,
    component: 'Input',
    show: false,
  },
];

async function initUsers() {
  const ret = await listUserByDeptId(103);
  const options: Option[] = ret.map((user) => ({
    label: `${user.userName} | ${user.nickName}`,
    value: user.userId,
  }));
  return options;
}
