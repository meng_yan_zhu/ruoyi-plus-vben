import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';

export const formSchemas: FormSchema[] = [];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '权益名称',
    dataIndex: 'equityName',
    width: 240,
  },
  {
    title: '展示区域',
    dataIndex: 'equityTypeId',
    width: 100,
  },
  {
    title: '等待期',
    dataIndex: 'waitingDays',
    width: 80,
  },
  {
    title: '延长天数',
    dataIndex: 'extendDays',
    width: 80,
  },
  {
    title: '服务次数',
    dataIndex: 'serviceTimes',
    width: 80,
  },
  {
    title: '间隔天数',
    dataIndex: 'applyInterval',
    width: 80,
  },
  {
    title: '服务类别',
    dataIndex: 'serviceType',
    width: 100,
    customRender: ({ value }) => renderDict(value, 'ccp_service_type'),
  },
  {
    title: '限制类型',
    dataIndex: 'chooseOneLimit',
    width: 100,
    customRender: ({ value }) => renderDict(value, 'ccp_choose_one_limit'),
  },
  {
    title: '是否共享  ',
    dataIndex: 'share',
    width: 80,
    customRender: ({ value }) => renderDict(value, 'ccp_yes_no'),
  },
  {
    title: '天次',
    dataIndex: 'dayCount',
    width: 80,
  },
  {
    title: '绑定状态 ',
    dataIndex: 'bindStatus',
    width: 80,
    customRender: ({ value }) => renderDict(value, 'ccp_yes_no'),
  },
  {
    title: '创建者',
    dataIndex: 'createBy',
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: 'ID',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '卡ID',
    field: 'cardId',
    required: true,
    component: 'Input',
    show: false,
  },
  {
    label: '选择权益',
    field: 'equityId',
    required: true,
    component: 'Select',
  },
  {
    label: '展示区域',
    field: 'equityTypeId',
    helpMessage: ['前端服务聚合页分类展示标签'],
    required: true,
    component: 'Select',
  },
  {
    label: '等待期',
    field: 'waitingDays',
    helpMessage: ['用户激活后需等待的天数'],
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 1800,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '延长天数',
    field: 'extendDays',
    helpMessage: [
      '用户激活时旧卡有申请该权益',
      '新卡该权益的生效时间 = 激活时间 + 等待期 + 延长天数',
    ],
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '服务次数',
    field: 'serviceTimes',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 365,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '间隔天数',
    field: 'applyInterval',
    helpMessage: ['用户申请该权益后，间隔XX天后可继续申请'],
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '服务类别',
    field: 'serviceType',
    helpMessage: ['总次数：一共可申请的次数', '每年次数：每年可申请的次数（每年重置）'],
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_service_type'),
    },
  },
  {
    label: '多选一限制类型',
    field: 'chooseOneLimit',
    required: true,
    helpMessage: ['按年限制：多选一权益每年可申请一次', '终身限制：多选一权益只能申请一次'],
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_choose_one_limit'),
    },
  },
  {
    label: '多选一权益',
    field: 'chooseOne',
    helpMessage: ['有效期内只能享受一种权益'],
    required: false,
    component: 'Select',
  },
  {
    label: '不可同时申请权益',
    field: 'notParallelApply',
    helpMessage: ['选中的权益不可并行申请'],
    required: false,
    component: 'Select',
  },
  {
    label: '是否共享',
    field: 'share',
    helpMessage: ['是否和其他服务共享天次'],
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
  },
  {
    label: '共享天次',
    field: 'dayCount',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
    },
    ifShow: ({ values }) => {
      return values.share === '1';
    },
  },
];
