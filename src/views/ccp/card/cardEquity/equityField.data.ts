import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { useRender } from '@/hooks/component/useRender';
import { VxeGridPropTypes, VxeTablePropTypes } from '@/components/VxeTable';
import { Input, Checkbox, Select, InputNumber } from 'ant-design-vue';
import { reactive, h } from 'vue';
import { dictOptionSelectList } from '@/api/system/dict/dictType';
import { getPopupContainer } from '@/utils';
import { getDictOptions } from '@/utils/dict';

export const formSchemas: FormSchema[] = [
  {
    field: 'labelName',
    component: 'Input',
    label: '字段名称',
  },
];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '字段名称',
    dataIndex: 'labelName',
    width: 120,
  },
  {
    title: '字段变量',
    dataIndex: 'variableName',
    width: 120,
  },
  {
    title: '控件类型',
    dataIndex: 'fieldType',
    width: 120,
    customRender: ({ value }) => renderDict(value, 'ccp_field_type'),
  },
  {
    title: '输入提示',
    dataIndex: 'inputHint',
    width: 120,
    ellipsis: true,
  },
];

const queryTypeOptions = [
  { label: '=', value: 'EQ' },
  { label: '!=', value: 'NE' },
  { label: '>', value: 'GT' },
  { label: '>=', value: 'GE' },
  { label: '<', value: 'LT' },
  { label: '<=', value: 'LE' },
  { label: 'LIKE', value: 'LIKE' },
  { label: 'BETWEEN', value: 'BETWEEN' },
];

const componentsOptions = getDictOptions('ccp_field_type');

const readOnlyOptions = getDictOptions('ccp_read_only_type');

const dictOptions = reactive<{ label: string; value: string }[]>([{ label: '未设置', value: '' }]);
/**
 * 在这里初始化字典下拉框
 */
(async function init() {
  const ret = await dictOptionSelectList();

  ret.forEach((dict) => {
    const option = {
      label: `${dict.dictName} | ${dict.dictType}`,
      value: dict.dictType,
    };
    dictOptions.push(option);
  });
})();

function renderBooleanTag(row: Recordable, field: string) {
  const value = row[field] ? '是' : '否';
  const className = row[field] ? 'text-green-500' : 'text-red-500';
  return h('span', { class: className }, value);
}

function renderBooleanCheckbox(row: Recordable, field: string) {
  return h(Checkbox, {
    checked: row[field],
    'onUpdate:checked': (checked) => {
      row[field] = checked;
    },
  });
}

export const validRules: VxeTablePropTypes.EditRules = {
  columnComment: [{ required: true, message: '请输入' }],
  javaField: [{ required: true, message: '请输入' }],
};

export const vxeTableColumns: VxeGridPropTypes.Columns = [
  {
    title: '序号',
    type: 'seq',
    fixed: 'left',
    field: 'sort',
    width: '50',
    align: 'center',
  },
  {
    title: '字段名称',
    field: 'labelName',
    showOverflow: 'tooltip',
    fixed: 'left',
    width: '120',
  },
  {
    title: '字段变量',
    field: 'variableName',
  },
  {
    title: '控件类型',
    field: 'fieldType',
    showOverflow: 'tooltip',
    slots: {
      default: ({ row }) => {
        const fieldType = row.fieldType;
        const found = componentsOptions.find((item) => item.value === fieldType);
        if (found) {
          return found.label;
        }
        return fieldType;
      },
      // edit: ({ row }) => {
      //   return h(Select, {
      //     value: row.fieldType,
      //     options: componentsOptions,
      //     getPopupContainer,
      //     'onUpdate:value': (value) => {
      //       row.fieldType = value;
      //     },
      //   });
      // },
    },
  },
  {
    title: '输入提示',
    field: 'inputHint',
    width: 150,
    slots: {
      edit: ({ row }) => {
        return h(Input, {
          value: row.inputHint,
          'onUpdate:value': (value) => {
            row.inputHint = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    title: '排序',
    field: 'sort',
    width: 80,
    showOverflow: 'tooltip',
    align: 'center',
    sortable: true,
    slots: {
      edit: ({ row }) => {
        return h(InputNumber, {
          value: row.sort,
          'onUpdate:value': (value) => {
            row.sort = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    title: '必填',
    field: 'required',
    width: 80,
    showOverflow: 'tooltip',
    align: 'center',
    slots: {
      default: ({ row }) => {
        return renderBooleanTag(row, 'required');
      },
      edit: ({ row }) => {
        return renderBooleanCheckbox(row, 'required');
      },
    },
    editRender: {},
  },
  {
    title: '只读',
    field: 'readOnly',
    showOverflow: 'tooltip',
    align: 'center',
    width: 120,
    slots: {
      default: ({ row }) => {
        const readOnly = row.readOnly;
        const found = readOnlyOptions.find((item) => item.value === readOnly);
        if (found) {
          return found.label;
        }
        return readOnly;
      },
      edit: ({ row }) => {
        return h(Select, {
          value: row.readOnly,
          options: readOnlyOptions,
          getPopupContainer,
          'onUpdate:value': (value) => {
            row.readOnly = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    title: '是否回传',
    field: 'postback',
    showOverflow: 'tooltip',
    align: 'center',
    width: 80,
    slots: {
      default: ({ row }) => {
        return renderBooleanTag(row, 'postback');
      },
      edit: ({ row }) => {
        return renderBooleanCheckbox(row, 'postback');
      },
    },
    editRender: {},
  },
  {
    title: '是否服务地址',
    field: 'isServiceAddress',
    showOverflow: 'tooltip',
    align: 'center',
    width: 80,
    slots: {
      default: ({ row }) => {
        return renderBooleanTag(row, 'isServiceAddress');
      },
      edit: ({ row }) => {
        return renderBooleanCheckbox(row, 'isServiceAddress');
      },
    },
    editRender: {},
  },
  {
    title: '回写字段',
    field: 'writeField',
    showOverflow: 'tooltip',
    align: 'center',
    width: 150,
    slots: {
      default: ({ row }) => {
        const queryType = row.queryType;
        const found = queryTypeOptions.find((item) => item.value === queryType);
        if (found) {
          return found.label;
        }
        return queryType;
      },
      edit: ({ row }) => {
        return h(Select, {
          value: row.queryType,
          options: queryTypeOptions,
          getPopupContainer,
          'onUpdate:value': (value) => {
            row.queryType = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    width: 80,
    title: '操作',
    align: 'center',
    slots: { default: 'action' },
  },
];
