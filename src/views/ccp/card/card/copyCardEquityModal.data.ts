import { FormSchema } from '@/components/Form';
import { VxeGridPropTypes, VxeTablePropTypes } from '@/components/VxeTable';
import { h, ref } from 'vue';
import { Button, Select } from 'ant-design-vue';
import { cardEquityListAll } from '@/api/ccp/cardEquity';

export const validRules: VxeTablePropTypes.EditRules = {
  columnComment: [{ required: true, message: '请输入' }],
  javaField: [{ required: true, message: '请输入' }],
};

export const sourceEquityOptions = ref<any>([]);

export const targetEquityOptions = ref<any>([]);

export async function updateEquityOptions(cardId: string, type: string) {
  try {
    const ret = await cardEquityListAll({ cardId: cardId });
    if (type === 'sourceCard') {
      sourceEquityOptions.value = ret.map((cardEquity) => ({
        label: `${cardEquity.equityName}`,
        value: cardEquity.id,
      }));
    } else {
      targetEquityOptions.value = ret.map((cardEquity) => ({
        label: `${cardEquity.equityName}`,
        value: cardEquity.id,
      }));
    }
  } catch (error) {
    console.error('Failed to fetch equity options:', error);
  }
}

export const schemas: FormSchema[] = [
  {
    label: '源卡片',
    field: 'sourceCard',
    component: 'Select',
    required: true,
    componentProps: {
      allowClear: false,
      options: [],
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '目标卡片',
    field: 'targetCard',
    required: true,
    component: 'Select',
    colProps: {
      span: 12,
    },
  },
];

export const vxeTableColumns: VxeGridPropTypes.Columns = [
  {
    type: 'checkbox',
    align: 'center',
    width: 60,
  },
  {
    title: '源权益',
    field: 'sourceEquity',
    slots: {
      default: ({ row }) => {
        const sourceEquity = row.sourceEquity;
        const found = sourceEquityOptions.value.find((item) => item.value === sourceEquity);
        if (found) {
          return found.label;
        }
        return sourceEquity;
      },
      edit: ({ row }) => {
        return h(Select, {
          value: row.sourceEquity,
          options: sourceEquityOptions.value,
          'onUpdate:value': (value) => {
            row.sourceEquity = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    title: '目标权益',
    field: 'targetEquity',
    slots: {
      default: ({ row }) => {
        const targetEquity = row.targetEquity;
        const found = targetEquityOptions.value.find((item) => item.value === targetEquity);
        if (found) {
          return found.label;
        }
        return targetEquity;
      },
      edit: ({ row }) => {
        return h(Select, {
          value: row.targetEquity,
          options: targetEquityOptions.value,
          'onUpdate:value': (value) => {
            row.targetEquity = value;
          },
        });
      },
    },
    editRender: {},
  },
  {
    width: 80,
    title: '操作',
    align: 'center',
    slots: {
      default: ({ $table, row }) => {
        function removeRow() {
          $table.remove(row);
        }
        return h(
          Button,
          {
            color: 'error',
            type: 'primary',
            danger: true,
            ghost: true,
            onClick: removeRow,
          },
          '删除',
        );
      },
    },
  },
];
