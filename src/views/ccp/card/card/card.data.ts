import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';
import { cardClassListAll } from '@/api/ccp/cardClass';

export const formSchemas: FormSchema[] = [
  {
    label: '卡片名称',
    field: 'cardName',
    component: 'Input',
  },
  {
    label: '卡片类别',
    field: 'type',
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_card_type'),
    },
  },
  {
    label: '卡片年限',
    field: 'life',
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_card_life'),
    },
  },
];

const { renderDict, renderTooltip } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '卡片名称',
    dataIndex: 'cardName',
    width: 280,
  },
  {
    title: '卡片类别',
    dataIndex: 'type',
    width: 80,
    customRender: ({ value }) => renderDict(value, 'ccp_card_type'),
  },
  {
    title: '卡片年限',
    dataIndex: 'life',
    width: 80,
    customRender: ({ value }) => renderDict(value, 'ccp_card_life'),
  },
  {
    title: '卡面图片',
    dataIndex: 'cover',
    width: 100,
    ellipsis: true,
    customRender({ value }) {
      return renderTooltip(value);
    },
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: '基础信息',
    field: 'baseInfo',
    component: 'Divider',
    componentProps: {
      orientation: 'left',
      plain: false,
    },
  },
  {
    label: 'ID',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '所属卡类',
    field: 'cardClassId',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: cardClassListAll,
      labelField: 'cardClassName',
      valueField: 'id',
      allowClear: false,
      disabled: true,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '卡片名称',
    field: 'cardName',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '卡片类别',
    field: 'type',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_card_type'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '卡片年限',
    field: 'life',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_card_life'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '绑定人数',
    field: 'bindNumLimit',
    required: true,
    component: 'InputNumber',
    helpMessage: '不含主权益人',
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.type !== '1';
    },
  },
  {
    label: '绑定期限',
    field: 'bindPeriod',
    required: true,
    helpMessage: '卡片激活后，限制XX天之内可绑定家庭成员',
    component: 'InputNumber',
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.type !== '1';
    },
  },
  {
    label: '卡号前缀',
    field: 'cardNoPrefix',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '销售终端',
    field: 'channelType',
    required: true,
    component: 'Select',
    colProps: {
      span: 12,
    },
  },
  {
    label: '开启弹窗',
    field: 'openHint',
    required: true,
    helpMessage: '激活时弹窗提示',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '弹窗内容',
    field: 'hintMessage',
    required: true,
    component: 'InputTextArea',
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.openHint !== '0';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '是否去重',
    field: 'ifRepeat',
    required: true,
    helpMessage: '开启此选项，一人可激活多张该卡片，卡类如果也开启，则执行一人多卡逻辑',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '年期规则',
    field: 'yearRule',
    required: true,
    helpMessage: [
      '无规则：',
      '申请后冻结其他已激活卡片',
      '一年期规则：',
      '1.先申请1年期，不可使用1年期；2.先申请1年期，可使用多年期；3.先申请多年期，不可使用1年期；4.先申请多年期，不可使用多年期',
    ],
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.ifRepeat !== '1';
    },
  },
  {
    label: '强制激活',
    field: 'forceActivate',
    required: true,
    helpMessage: '开启此选项，有重复的激活人时，提示用户选择激活新卡，废弃旧卡',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.ifRepeat !== '0';
    },
  },
  {
    label: '生效逻辑',
    field: 'effectiveLogic',
    required: true,
    helpMessage: [
      '生效逻辑不执行：新卡激活其他不变',
      '生效逻辑执行：检测旧卡的权益，有申请：激活时间 + 等待期 + 延长天数；无申请：老卡权益开始时间',
    ],
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '生效开关',
    field: 'effectiveSwitch',
    required: true,
    helpMessage: ['开启开关：有申请：激活时间 + 等待期 + 延长天数；无申请：老卡权益开始时间'],
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.effectiveLogic !== '0';
    },
  },
  {
    label: '有无保险公司',
    field: 'hasInsuranceCompany',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '激活最小年龄',
    field: 'activateMinAge',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 1800,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '激活最大年龄',
    field: 'activateMaxAge',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 1800,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '绑定最小年龄',
    field: 'bindMinAge',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 1800,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '绑定最大年龄',
    field: 'bindMaxAge',
    required: true,
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 1800,
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '展示信息',
    field: 'showInfo',
    component: 'Divider',
    componentProps: {
      orientation: 'left',
      plain: false,
    },
  },
  {
    label: '卡面图片',
    field: 'cover',
    required: false,
    component: 'ImageUpload',
    colProps: {
      span: 8,
    },
    /**
     * 注意这里获取为数组 需要自行定义回显/提交
     */
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 建议使用拓展名
      maxNumber: 1, // 最大上传文件数
      resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
  },
  {
    label: '头部背景',
    field: 'headBg',
    required: false,
    component: 'ImageUpload',
    colProps: {
      span: 8,
    },
    /**
     * 注意这里获取为数组 需要自行定义回显/提交
     */
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 建议使用拓展名
      maxNumber: 1, // 最大上传文件数
      resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
  },
  {
    label: '服务背景',
    field: 'serviceBg',
    required: false,
    component: 'ImageUpload',
    colProps: {
      span: 8,
    },
    /**
     * 注意这里获取为数组 需要自行定义回显/提交
     */
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 建议使用拓展名
      maxNumber: 1, // 最大上传文件数
      resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
  },
];
