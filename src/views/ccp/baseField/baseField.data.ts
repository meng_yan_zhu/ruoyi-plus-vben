import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';

export const formSchemas: FormSchema[] = [
  {
    label: '字段名称',
    field: 'labelName',
    component: 'Input',
  },
  {
    label: '字段变量',
    field: 'variableName',
    component: 'Input',
  },
  {
    label: '字段所属',
    field: 'type',
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_field_class'),
    },
  },
];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '字段名称',
    dataIndex: 'labelName',
  },
  {
    title: '字段变量',
    dataIndex: 'variableName',
  },
  {
    title: '字段所属',
    dataIndex: 'type',
    customRender: ({ value }) => renderDict(value, 'ccp_field_class'),
  },
  {
    title: '控件类型',
    dataIndex: 'fieldType',
    customRender: ({ value }) => renderDict(value, 'ccp_field_type'),
  },
  {
    title: '输入提示',
    dataIndex: 'inputHint',
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: 'ID',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '字段名称',
    field: 'labelName',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '字段变量',
    field: 'variableName',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '字段所属',
    field: 'type',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_field_class'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '输入提示',
    field: 'inputHint',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '控件类型',
    field: 'fieldType',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_field_type'),
    },
    colProps: {
      span: 12,
    },
  },

  {
    label: '来源类型',
    field: 'fromType',
    required: true,
    defaultValue: '0',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_from_type'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '接口路径',
    field: 'apiUrl',
    required: true,
    component: 'Input',
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fromType === '2';
    },
  },
  {
    label: '控件值来源',
    field: 'fieldDictValueFrom',
    required: true,
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fromType === '1';
    },
    component: 'Select',
    colProps: {
      span: 12,
    },
  },
  {
    label: '控件值',
    field: 'fieldValue',
    required: true,
    component: 'Select',
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fromType === '1';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '初始值来源',
    field: 'initialValueFrom',
    required: true,
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fromType === '1';
    },
    component: 'Select',
    colProps: {
      span: 12,
    },
  },
  {
    label: '初始值',
    field: 'initialValue',
    required: true,
    component: 'Select',
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fromType === '1';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '日期类型',
    field: 'dateFormat',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_date_format_type'),
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fieldType === 'datetime';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '级联类型',
    field: 'cascaderType',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_cascader_type'),
    },
    ifShow: ({ values }) => {
      // 类型不为按钮时显示
      return values.fieldType === 'cascader';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '提示框类型',
    field: 'tooltipType',
    required: true,
    defaultValue: '0',
    component: 'RadioButtonGroup',
    componentProps({ formActionType }) {
      return {
        options: getDictOptions('ccp_tooltip_type'),
        // 切换后清空校验结果
        onChange: async (e) => {
          // 拿到的可能是Event || string
          if (typeof e !== 'string') {
            return;
          }
          const type = e as string;
          const imageUploadComponentProps = ({ formActionType }) => {
            return {
              maxNumber: 3, // 最大上传文件数
              resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
              onChange(e) {
                console.log(e);
                formActionType.setFieldsValue({ tooltipContent: e.value });
              },
            };
          };
          await formActionType.updateSchema({
            field: 'tooltipContent',
            component: type === '2' ? 'ImageUpload' : 'InputTextArea',
            componentProps: type === '2' ? {} : imageUploadComponentProps,
          });
        },
      };
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '提示框内容',
    field: 'tooltipContent',
    required: true,
    component: 'InputTextArea',
    ifShow: ({ values }) => {
      return values.tooltipType !== '0';
    },
    /**
     * 注意 好像没啥可写的
     */
    componentProps: {
      // 没啥可写的
    },
    colProps: {
      span: 12,
    },
  },
];
