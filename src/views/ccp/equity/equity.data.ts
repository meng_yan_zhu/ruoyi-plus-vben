import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';
import { cardClassListAll } from '@/api/ccp/cardClass';

export const formSchemas: FormSchema[] = [
  {
    label: '所属卡类',
    field: 'cardClassId',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: cardClassListAll,
      labelField: 'cardClassName',
      valueField: 'id',
    },
  },
  {
    label: '权益名称',
    field: 'equityName',
    component: 'Input',
  },
  {
    label: '服务类型',
    field: 'useEquityType',
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_use_equity_type'),
    },
  },
  {
    label: '启用状态',
    field: 'isEnable',
    component: 'Select',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
  },
];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '权益名称',
    dataIndex: 'equityName',
    width: 300,
  },
  {
    title: '服务类型',
    dataIndex: 'useEquityType',
    width: 100,
    customRender: ({ value }) => renderDict(value, 'ccp_use_equity_type'),
  },
  {
    title: '权益概要',
    dataIndex: 'outline',
  },
  {
    title: '启用状态',
    dataIndex: 'isEnable',
    customRender: ({ value }) => renderDict(value, 'sys_normal_disable'),
  },
  {
    title: '复审要求',
    dataIndex: 'reviewRequire',
    customRender: ({ value }) => renderDict(value, 'ccp_review_require'),
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '创建人',
    dataIndex: 'createBy',
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    sorter: true,
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: 'equity_id',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '所属卡类',
    field: 'cardClassId',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: cardClassListAll,
      labelField: 'cardClassName',
      valueField: 'id',
      allowClear: false,
      disabled: true,
    },
  },
  {
    label: '权益名称',
    field: 'equityName',
    required: true,
    component: 'Input',
    colProps: {
      span: 12,
    },
  },
  {
    label: '服务类型',
    field: 'useEquityType',
    required: true,
    component: 'Select',
    componentProps: {
      options: getDictOptions('ccp_use_equity_type'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '小程序',
    field: 'appConfigId',
    required: false,
    component: 'Input',
    ifShow: ({ values }) => {
      return values.useEquityType === '2';
    },
  },
  {
    label: '外链路径',
    field: 'path',
    required: false,
    component: 'Input',
    ifShow: ({ values }) => {
      return values.useEquityType === '3';
    },
  },
  {
    label: '启用状态',
    field: 'isEnable',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '复审要求',
    field: 'reviewRequire',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_review_require'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '权益图标',
    field: 'img',
    required: true,
    component: 'ImageUpload',
    colProps: {
      span: 12,
    },
    /**
     * 注意这里获取为数组 需要自行定义回显/提交
     */
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 建议使用拓展名
      maxNumber: 1, // 最大上传文件数
      resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
  },
  {
    label: '权益背景',
    field: 'equityBg',
    required: true,
    component: 'ImageUpload',
    colProps: {
      span: 12,
    },
    /**
     * 注意这里获取为数组 需要自行定义回显/提交
     */
    componentProps: {
      // accept: ['jpg'], // 不支持type/*的写法 建议使用拓展名
      maxNumber: 1, // 最大上传文件数
      resultField: 'url', // 上传成功后返回的字段名 默认url 可选['ossId', 'url', 'fileName']
    },
  },
  {
    label: '权益概要',
    field: 'outline',
    required: true,
    component: 'Input',
  },
  {
    label: '权益描述',
    field: 'serviceDescribe',
    required: true,
    component: 'RichTextarea',
  },
  {
    label: '服务项目',
    field: 'serviceItems',
    required: true,
    component: 'RichTextarea',
    /**
     * 注意 好像没啥可写的
     */
    componentProps: {
      // 没啥可写的
    },
  },
  {
    label: '服务内容',
    field: 'serviceContent',
    required: true,
    component: 'RichTextarea',
    /**
     * 注意 好像没啥可写的
     */
    componentProps: {
      // 没啥可写的
    },
  },
  {
    label: '服务流程',
    field: 'serviceProcess',
    required: true,
    component: 'RichTextarea',
    /**
     * 注意 好像没啥可写的
     */
    componentProps: {
      // 没啥可写的
    },
  },
  {
    label: '注意事项',
    field: 'serviceNote',
    required: true,
    component: 'RichTextarea',
    /**
     * 注意 好像没啥可写的
     */
    componentProps: {
      // 没啥可写的
    },
  },
  {
    label: '备注',
    field: 'remark',
    required: false,
    component: 'InputTextArea',
  },
];
