import { FormSchema } from '@/components/Form';
// import { useRender } from '@/hooks/component/useRender';
import { cardClassListAll } from '@/api/ccp/cardClass';

// const { renderDict } = useRender();

export const schemas: FormSchema[] = [
  {
    label: '所属卡类',
    field: 'cardClassId',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: cardClassListAll,
      labelField: 'cardClassName',
      valueField: 'id',
      allowClear: false,
    },
  },
];
