import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';

export const formSchemas: FormSchema[] = [
  {
    label: '名称',
    field: 'name',
    component: 'Input',
  },
  {
    label: '启用状态',
    field: 'enableStatus',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
  },
  {
    label: '证书判断',
    field: 'cert',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_yes_no'),
    },
  },
];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'name',
    width: '15%',
  },
  {
    title: '启用状态',
    dataIndex: 'enableStatus',
    width: '5%',
    customRender: ({ value }) => renderDict(value, 'sys_normal_disable'),
  },
  {
    title: '弹窗',
    dataIndex: 'popUp',
    width: '5%',
    customRender: ({ value }) => renderDict(value, 'ccp_yes_no'),
  },
  {
    title: '证书判断',
    dataIndex: 'cert',
    width: '5%',
    customRender: ({ value }) => renderDict(value, 'ccp_yes_no'),
  },
  {
    title: '上户照片限制',
    dataIndex: 'homeVisitImg',
    width: '10%',
    customRender: ({ record }) => '≥' + record.homeVisitImgMin + ',≤' + record.homeVisitImgMax,
  },
  {
    title: '护工证书限制',
    dataIndex: 'nurseCertImg',
    width: '10%',
    customRender: ({ record }) => '≥' + record.nurseCertImgMin + ',≤' + record.nurseCertImgMax,
  },
  {
    title: '结算方式',
    dataIndex: 'paymentMethod',
    width: '10%',
    customRender: ({ value }) => renderDict(value, 'ccp_payment_method'),
  },
  {
    title: '固定时间',
    dataIndex: 'fixedTime',
    width: '10%',
  },
  {
    title: '起点时间',
    dataIndex: 'customTime',
    width: '10%',
    customRender: ({ value }) => renderDict(value, 'ccp_custom_time'),
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: 'id',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '甲方类型 1-权益甲方 2-服务甲方',
    field: 'type',
    required: true,
    component: 'Select',
    show: false,
  },
  {
    label: '名称',
    field: 'name',
    required: true,
    component: 'Input',
  },
  {
    label: '启用状态',
    field: 'enableStatus',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
  },
  {
    label: '弹窗',
    field: 'popUp',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '证书判断',
    field: 'cert',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '上户照片',
    field: 'homeVisitImgMin',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '上户照片',
    field: 'homeVisitImgMax',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '上户照片',
    field: 'homeVisitImg',
    required: true,
    slot: 'homeVisitImg',
  },
  {
    label: '护工证书最小数量',
    field: 'nurseCertImgMin',
    required: true,
    component: 'InputNumber',
    show: false,
  },
  {
    label: '护工证书最大数量',
    field: 'nurseCertImgMax',
    required: true,
    component: 'InputNumber',
    show: false,
  },
  {
    label: '护工证书',
    field: 'homeVisitImg',
    required: true,
    slot: 'nurseCertImg',
  },
  {
    label: '结算方式',
    field: 'paymentMethod',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_payment_method'),
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '固定时间',
    field: 'fixedTime',
    required: true,
    component: 'TimePicker',
    componentProps: {
      showTime: true,
      format: 'HH:mm',
      valueFormat: 'HH:mm',
    },
    ifShow: ({ values }) => {
      return values.paymentMethod === '1';
    },
    colProps: {
      span: 12,
    },
  },
  {
    label: '选择时间',
    field: 'customTime',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_custom_time'),
    },
    ifShow: ({ values }) => {
      return values.paymentMethod === '2';
    },
    colProps: {
      span: 12,
    },
  },
];
