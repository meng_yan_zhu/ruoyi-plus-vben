import { BasicColumn } from '@/components/Table';
import { FormSchema } from '@/components/Form';
import { getDictOptions } from '@/utils/dict';
import { useRender } from '@/hooks/component/useRender';

export const formSchemas: FormSchema[] = [
  {
    label: '名称',
    field: 'name',
    component: 'Input',
  },
  {
    label: '启用状态',
    field: 'enableStatus',
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
  },
];

const { renderDict } = useRender();
export const columns: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '启用状态',
    dataIndex: 'enableStatus',
    customRender: ({ value }) => renderDict(value, 'sys_normal_disable'),
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
  },
];

export const modalSchemas: FormSchema[] = [
  {
    label: 'id',
    field: 'id',
    required: false,
    component: 'Input',
    show: false,
  },
  {
    label: '甲方类型 1-权益甲方 2-服务甲方',
    field: 'type',
    required: true,
    component: 'Select',
    show: false,
  },
  {
    label: '名称',
    field: 'name',
    required: true,
    component: 'Input',
  },
  {
    label: '启用状态',
    field: 'enableStatus',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_normal_disable'),
    },
  },
  {
    label: '弹窗',
    field: 'popUp',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('ccp_yes_no'),
    },
  },
  {
    label: '证书判断',
    field: 'cert',
    required: true,
    component: 'RadioButtonGroup',
    componentProps: {
      options: getDictOptions('sys_yes_no'),
    },
  },
  {
    label: '上户照片最小数量',
    field: 'homeVisitImgMin',
    required: true,
    component: 'Input',
  },
  {
    label: '上户照片最大数量',
    field: 'homeVisitImgMax',
    required: true,
    component: 'Input',
  },
  {
    label: '护工证书最小数量',
    field: 'nurseCertImgMin',
    required: true,
    component: 'Input',
  },
  {
    label: '护工证书最大数量',
    field: 'nurseCertImgMax',
    required: true,
    component: 'Input',
  },
  {
    label: '结算方式 1-固定点 2-点对点',
    field: 'paymentMethod',
    required: true,
    component: 'Input',
  },
  {
    label: '固定时间',
    field: 'fixedTime',
    required: true,
    component: 'DatePicker',
    componentProps: {
      showTime: true,
      format: 'YYYY-MM-DD HH:mm:ss',
      valueFormat: 'YYYY-MM-DD HH:mm:ss',
    },
  },
  {
    label: '选择时间 1-上户时间 2-派单时间',
    field: 'customTime',
    required: true,
    component: 'Input',
  },
];
