export interface TabItem {
  key: string;
  name: string;
  component: string;
}

export const tabListData: TabItem[] = [
  {
    key: '2',
    name: '服务甲方',
    component: 'ServiceSource',
  },
  {
    key: '1',
    name: '权益甲方',
    component: 'EquitySource',
  },
];
