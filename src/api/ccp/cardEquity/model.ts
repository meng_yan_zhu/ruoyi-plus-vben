import { BaseEntity, PageQuery } from '@/api/base';

export interface CardEquityVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 卡ID
   */
  cardId: string | number;

  /**
   * 卡片名称
   */
  cardName: string;

  /**
   * 权益ID
   */
  equityId: string | number;

  /**
   * 权益名称
   */
  equityName: string;

  /**
   * 权益分类ID->展示区域
   */
  equityTypeId: string | number;

  /**
   * 等待期（天）
   */
  waitingDays: number;

  /**
   * 延长天数
   */
  extendDays: number;

  /**
   * 服务次数
   */
  serviceTimes: string;

  /**
   * 间隔天数
   */
  applyInterval: number;

  /**
   * 服务类别,(1:每年次数; 2:总次数)
   */
  serviceType: string;

  /**
   * 多选一
   */
  chooseOne: any;

  /**
   * 限制类型
   */
  chooseOneLimit: number;

  /**
   * 不能并行申请的权益列表
   */
  notParallelApply: any;

  /**
   * 是否共享  （0 否 1 是）
   */
  share: number;

  /**
   * 天次
   */
  dayCount: number;

  /**
   * 绑定状态 （0 未绑定 1 已绑定）
   */
  bindStatus: number;

  /**
   * 表单字段
   */
  equityFields: string;

  /**
   * 删除标志（0代表存在 1代表删除）
   */
  dataStatus: number;

  /**
   * 创建者
   */
  createBy: string;

  /**
   * 创建时间
   */
  createTime: string;
}

export interface CardEquityForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 卡ID
   */
  cardId?: string | number;

  /**
   * 权益ID
   */
  equityId?: string | number;

  /**
   * 权益分类ID->展示区域
   */
  equityTypeId?: string | number;

  /**
   * 等待期（天）
   */
  waitingDays?: number;

  /**
   * 延长天数
   */
  extendDays?: number;

  /**
   * 服务次数
   */
  serviceTimes?: string;

  /**
   * 间隔天数
   */
  applyInterval?: number;

  /**
   * 服务类别,(1:每年次数; 2:总次数)
   */
  serviceType?: string;

  /**
   * 多选一
   */
  chooseOne?: string;

  /**
   * 限制类型
   */
  chooseOneLimit?: number;

  /**
   * 不能并行申请的权益列表
   */
  notParallelApply?: string;

  /**
   * 是否共享  （0 否 1 是）
   */
  share?: number;

  /**
   * 天次
   */
  dayCount?: number;

  /**
   * 绑定状态 （0 未绑定 1 已绑定）
   */
  bindStatus?: number;

  /**
   * 表单字段
   */
  equityFields?: string;

  /**
   * 删除标志（0代表存在 1代表删除）
   */
  dataStatus?: number;
}

export interface CardEquityQuery extends PageQuery {
  /**
   * 卡ID
   */
  cardId?: string | number;

  /**
   * 日期范围参数
   */
  params?: any;
}
