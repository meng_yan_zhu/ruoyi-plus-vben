import { defHttp } from '@/utils/http/axios';
import { ID, IDS, commonExport } from '@/api/base';
import { CardEquityVO, CardEquityForm, CardEquityQuery } from './model';

/**
 * 查询卡片权益列表
 * @param params
 * @returns
 */
export function cardEquityList(params?: CardEquityQuery) {
  return defHttp.get<CardEquityVO[]>({ url: '/ccp/cardEquity/list', params });
}

/**
 * 查询卡片权益列表
 * @param params
 * @returns
 */
export function cardEquityListAll(params?: CardEquityQuery) {
  return defHttp.get<CardEquityVO[]>({ url: '/ccp/cardEquity/listAll', params });
}

/**
 * 导出卡片权益列表
 * @param params
 * @returns
 */
export function cardEquityExport(params?: CardEquityQuery) {
  return commonExport('/ccp/cardEquity/export', params ?? {});
}

/**
 * 查询卡片权益详细
 * @param id id
 * @returns
 */
export function cardEquityInfo(id: ID) {
  return defHttp.get<CardEquityVO>({ url: '/ccp/cardEquity/' + id });
}

/**
 * 新增卡片权益
 * @param data
 * @returns
 */
export function cardEquityAdd(data: CardEquityForm) {
  return defHttp.postWithMsg<void>({ url: '/ccp/cardEquity', data });
}

/**
 * 更新卡片权益
 * @param data
 * @returns
 */
export function cardEquityUpdate(data: CardEquityForm) {
  return defHttp.putWithMsg<void>({ url: '/ccp/cardEquity', data });
}

/**
 * 删除卡片权益
 * @param id id
 * @returns
 */
export function cardEquityRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/ccp/cardEquity/' + id });
}
