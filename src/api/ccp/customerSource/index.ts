import { defHttp } from '@/utils/http/axios';
import { ID, IDS, commonExport } from '@/api/base';
import { CustomerSourceVO, CustomerSourceForm, CustomerSourceQuery } from './model';

/**
 * 查询甲方配置列表
 * @param params
 * @returns
 */
export function customerSourceList(params?: CustomerSourceQuery) {
  return defHttp.get<CustomerSourceVO[]>({ url: '/ccp/customerSource/list', params });
}

/**
 * 导出甲方配置列表
 * @param params
 * @returns
 */
export function customerSourceExport(params?: CustomerSourceQuery) {
  return commonExport('/ccp/customerSource/export', params ?? {});
}

/**
 * 查询甲方配置详细
 * @param id id
 * @returns
 */
export function customerSourceInfo(id: ID) {
  return defHttp.get<CustomerSourceVO>({ url: '/ccp/customerSource/' + id });
}

/**
 * 新增甲方配置
 * @param data
 * @returns
 */
export function customerSourceAdd(data: CustomerSourceForm) {
  return defHttp.postWithMsg<void>({ url: '/ccp/customerSource', data });
}

/**
 * 更新甲方配置
 * @param data
 * @returns
 */
export function customerSourceUpdate(data: CustomerSourceForm) {
  return defHttp.putWithMsg<void>({ url: '/ccp/customerSource', data });
}

/**
 * 删除甲方配置
 * @param id id
 * @returns
 */
export function customerSourceRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/ccp/customerSource/' + id });
}
