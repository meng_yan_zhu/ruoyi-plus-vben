import { BaseEntity, PageQuery } from '@/api/base';

export interface CustomerSourceVO {
  /**
   * id
   */
  id: string | number;

  /**
   * 甲方类型 1-权益甲方 2-服务甲方
   */
  type: number;

  /**
   * 项目名称
   */
  name: string;

  /**
   * 启用状态 0-禁用 1-启用
   */
  enableStatus: number;

  /**
   * 弹窗 0-关闭 1-开启
   */
  popUp: number;

  /**
   * 证书判断 0-关闭 1-开启
   */
  cert: number;

  /**
   * 额外字段
   */
  extraFields: string;

  /**
   * 上户照片最小数量
   */
  homeVisitImgMin: number;

  /**
   * 上户照片最大数量
   */
  homeVisitImgMax: number;

  /**
   * 护工证书最小数量
   */
  nurseCertImgMin: number;

  /**
   * 护工证书最大数量
   */
  nurseCertImgMax: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus: number;

  /**
   * 结算方式 1-固定点 2-点对点
   */
  paymentMethod: number;

  /**
   * 固定时间
   */
  fixedTime: string;

  /**
   * 选择时间 1-上户时间 2-派单时间
   */
  customTime: number;
}

export interface CustomerSourceForm extends BaseEntity {
  /**
   * id
   */
  id?: string | number;

  /**
   * 甲方类型 1-权益甲方 2-服务甲方
   */
  type?: number;

  /**
   * 项目名称
   */
  name?: string;

  /**
   * 启用状态 0-禁用 1-启用
   */
  enableStatus?: number;

  /**
   * 弹窗 0-关闭 1-开启
   */
  popUp?: number;

  /**
   * 证书判断 0-关闭 1-开启
   */
  cert?: number;

  /**
   * 额外字段
   */
  extraFields?: string;

  /**
   * 上户照片最小数量
   */
  homeVisitImgMin?: number;

  /**
   * 上户照片最大数量
   */
  homeVisitImgMax?: number;

  /**
   * 护工证书最小数量
   */
  nurseCertImgMin?: number;

  /**
   * 护工证书最大数量
   */
  nurseCertImgMax?: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus?: number;

  /**
   * 结算方式 1-固定点 2-点对点
   */
  paymentMethod?: number;

  /**
   * 固定时间
   */
  fixedTime?: string;

  /**
   * 选择时间 1-上户时间 2-派单时间
   */
  customTime?: number;
}

export interface CustomerSourceQuery extends PageQuery {
  /**
   * 甲方类型 1-权益甲方 2-服务甲方
   */
  type?: number;

  /**
   * 项目名称
   */
  name?: string;

  /**
   * 启用状态 0-禁用 1-启用
   */
  enableStatus?: number;

  /**
   * 弹窗 0-关闭 1-开启
   */
  popUp?: number;

  /**
   * 证书判断 0-关闭 1-开启
   */
  cert?: number;

  /**
   * 额外字段
   */
  extraFields?: string;

  /**
   * 上户照片最小数量
   */
  homeVisitImgMin?: number;

  /**
   * 上户照片最大数量
   */
  homeVisitImgMax?: number;

  /**
   * 护工证书最小数量
   */
  nurseCertImgMin?: number;

  /**
   * 护工证书最大数量
   */
  nurseCertImgMax?: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus?: number;

  /**
   * 结算方式 1-固定点 2-点对点
   */
  paymentMethod?: number;

  /**
   * 固定时间
   */
  fixedTime?: string;

  /**
   * 选择时间 1-上户时间 2-派单时间
   */
  customTime?: number;

  /**
   * 日期范围参数
   */
  params?: any;
}
