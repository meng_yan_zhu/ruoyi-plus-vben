import { BaseEntity, PageQuery } from '@/api/base';

export interface EquityVO {
  /**
   * equity_id
   */
  id: string | number;

  /**
   * 卡类ID
   */
  cardClassId: string | number;

  /**
   * 卡类名称
   */
  cardClassName: string;

  /**
   * 名称
   */
  equityName: string;

  /**
   * 权益使用类型 1-服务类 2-小程序类 3-外链
   */
  useEquityType: number;

  /**
   * 小程序配置表ID
   */
  appConfigId: string | number;

  /**
   * 外链路径
   */
  path: string;

  /**
   * 权益概要
   */
  outline: string;

  /**
   * 权益描述
   */
  serviceDescribe: string;

  /**
   * 图标
   */
  img: string;

  /**
   * 服务项目
   */
  serviceItems: string;

  /**
   * 服务内容
   */
  serviceContent: string;

  /**
   * 服务流程
   */
  serviceProcess: string;

  /**
   * 注意事项
   */
  serviceNote: string;

  /**
   * 权益背景图
   */
  equityBg: string;

  /**
   * 启用状态,(0:启用; 1:禁用)
   */
  isEnable: string;

  /**
   * 复审要求(0无需复审 1需要复审)
   */
  reviewRequire: number;

  /**
   * 是否主责（0:非主责  1:主责)
   */
  isMainDuty: number;

  /**
   * 备注
   */
  remark: string;
}

export interface EquityForm extends BaseEntity {
  /**
   * equity_id
   */
  id?: string | number;

  /**
   * 卡类ID
   */
  cardClassId?: string | number;

  /**
   * 名称
   */
  equityName?: string;

  /**
   * 权益使用类型 1-服务类 2-小程序类 3-外链
   */
  useEquityType?: number;

  /**
   * 小程序配置表ID
   */
  appConfigId?: string | number;

  /**
   * 外链路径
   */
  path?: string;

  /**
   * 权益概要
   */
  outline?: string;

  /**
   * 权益描述
   */
  serviceDescribe?: string;

  /**
   * 图标
   */
  img?: string;

  /**
   * 服务项目
   */
  serviceItems?: string;

  /**
   * 服务内容
   */
  serviceContent?: string;

  /**
   * 服务流程
   */
  serviceProcess?: string;

  /**
   * 注意事项
   */
  serviceNote?: string;

  /**
   * 权益背景图
   */
  equityBg?: string;

  /**
   * 启用状态,(0:启用; 1:禁用)
   */
  isEnable?: string;

  /**
   * 复审要求(0无需复审 1需要复审)
   */
  reviewRequire?: number;

  /**
   * 是否主责（0:非主责  1:主责)
   */
  isMainDuty?: number;

  /**
   * 备注
   */
  remark?: string;
}

export interface EquityQuery extends PageQuery {
  /**
   * 卡类ID
   */
  cardClassId?: string | number;

  /**
   * 名称
   */
  equityName?: string;

  /**
   * 权益概要
   */
  outline?: string;

  /**
   * 启用状态,(0:启用; 1:禁用)
   */
  isEnable?: string;

  /**
   * 日期范围参数
   */
  params?: any;
}

export interface EquityCopy extends PageQuery {
  /**
   * 卡类ID
   */
  cardClassId?: string | number;

  equityList?: EquityVO[];
}
