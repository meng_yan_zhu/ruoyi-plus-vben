import { defHttp } from '@/utils/http/axios';
import { ID, IDS, commonExport } from '@/api/base';
import { EquityVO, EquityForm, EquityQuery, EquityCopy } from './model';

/**
 * 查询权益信息列表
 * @param params
 * @returns
 */
export function equityList(params?: EquityQuery) {
  return defHttp.get<EquityVO[]>({ url: '/ccp/equity/list', params });
}

/**
 * 查询权益信息列表
 * @param params
 * @returns
 */
export function equityListAll(params?: EquityQuery) {
  return defHttp.get<EquityVO[]>({ url: '/ccp/equity/listAll', params });
}

/**
 * 导出权益信息列表
 * @param params
 * @returns
 */
export function equityExport(params?: EquityQuery) {
  return commonExport('/ccp/equity/export', params ?? {});
}

/**
 * 查询权益信息详细
 * @param id id
 * @returns
 */
export function equityInfo(id: ID) {
  return defHttp.get<EquityVO>({ url: '/ccp/equity/' + id });
}

/**
 * 新增权益信息
 * @param data
 * @returns
 */
export function equityAdd(data: EquityForm) {
  return defHttp.postWithMsg<void>({ url: '/ccp/equity', data });
}

/**
 * 更新权益信息
 * @param data
 * @returns
 */
export function equityUpdate(data: EquityForm) {
  return defHttp.putWithMsg<void>({ url: '/ccp/equity', data });
}

/**
 * 删除权益信息
 * @param id id
 * @returns
 */
export function equityRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/ccp/equity/' + id });
}

/**
 * 复制权益信息到目标卡类
 * @returns
 * @param data
 */
export function equityCopy(data: EquityCopy) {
  return defHttp.postWithMsg<void>({ url: '/ccp/equity/copy', data });
}
