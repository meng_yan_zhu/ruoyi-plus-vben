import { defHttp } from '@/utils/http/axios';
import { ID, IDS } from '@/api/base';
import { CardClassVO, CardClassForm, CardClassQuery } from './model';

/**
 * 查询卡片种类列表
 * @param params
 * @returns
 */
export function cardClassList(params?: CardClassQuery) {
  return defHttp.get<CardClassVO[]>({ url: '/system/cardClass/list', params });
}

/**
 * 查询卡片种类列表
 * @param params
 * @returns
 */
export function cardClassListAll(params?: CardClassQuery) {
  return defHttp.get<CardClassVO[]>({ url: '/system/cardClass/listAll', params });
}

/**
 * 导出卡片种类列表
 * @param params
 * @returns
 */
export function cardClassExport(params?: CardClassQuery) {
  return defHttp.post<Blob>(
    { url: '/system/cardClass/export', params, responseType: 'blob' },
    { isTransformResponse: false },
  );
}

/**
 * 查询卡片种类详细
 * @param id id
 * @returns
 */
export function cardClassInfo(id: ID) {
  return defHttp.get<CardClassVO>({ url: '/system/cardClass/' + id });
}

/**
 * 新增卡片种类
 * @param data
 * @returns
 */
export function cardClassAdd(data: CardClassForm) {
  return defHttp.postWithMsg<void>({ url: '/system/cardClass', data });
}

/**
 * 更新卡片种类
 * @param data
 * @returns
 */
export function cardClassUpdate(data: CardClassForm) {
  return defHttp.putWithMsg<void>({ url: '/system/cardClass', data });
}

/**
 * 删除卡片种类
 * @param id id
 * @returns
 */
export function cardClassRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/system/cardClass/' + id });
}
