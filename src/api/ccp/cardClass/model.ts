import { BaseEntity, PageQuery } from '@/api/base';

export interface CardClassVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 卡类名称
   */
  cardClassName: string;

  /**
   * 卡面图片
   */
  cover: string;

  /**
   * 主题颜色
   */
  subjectColor: string;

  /**
   * 来源类型 1内部 2外部
   */
  sourceType: string;

  /**
   * 具体来源 1北京小橙 2天津小橙 3小橙集团 4荷叶 5长城
   */
  sourceTypeDetail: string;

  /**
   * 是否去重 1是 2否
   */
  ifRepeat: string;

  /**
   * 负责人
   */
  director: string;

  /**
   * 分区信息
   */
  partitionInfo: string;

  /**
   * 强制激活(1不允许2允许)
   */
  forceActivate: string;

  /**
   * 权益状态 1未配置 2已配置
   */
  equityStatus: string;

  /**
   * 零售商信息表ID
   */
  retailerId: string | number;

  /**
   * 校验绑定卡号 0否 1是
   */
  verifyBindCardNo: number;

  /**
   * 有无保险公司  0否 1是
   */
  hasInsuranceCompany: number;
}

export interface CardClassForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 卡类名称
   */
  cardClassName?: string;

  /**
   * 卡面图片
   */
  cover?: string;

  /**
   * 主题颜色
   */
  subjectColor?: string;

  /**
   * 来源类型 1内部 2外部
   */
  sourceType?: string;

  /**
   * 具体来源 1北京小橙 2天津小橙 3小橙集团 4荷叶 5长城
   */
  sourceTypeDetail?: string;

  /**
   * 是否去重 1是 2否
   */
  ifRepeat?: string;

  /**
   * 负责人
   */
  director?: string;

  /**
   * 分区信息
   */
  partitionInfo?: string;

  /**
   * 强制激活(1不允许2允许)
   */
  forceActivate?: string;

  /**
   * 权益状态 1未配置 2已配置
   */
  equityStatus?: string;

  /**
   * 零售商信息表ID
   */
  retailerId?: string | number;

  /**
   * 校验绑定卡号 0否 1是
   */
  verifyBindCardNo?: number;

  /**
   * 有无保险公司  0否 1是
   */
  hasInsuranceCompany?: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus?: number;
}

export interface CardClassQuery extends PageQuery {
  /**
   * 卡类名称
   */
  cardClassName?: string;

  /**
   * 来源类型 1内部 2外部
   */
  sourceType?: string;

  /**
   * 具体来源 1北京小橙 2天津小橙 3小橙集团 4荷叶 5长城
   */
  sourceTypeDetail?: string;

  /**
   * 负责人
   */
  director?: string;

  /**
   * 日期范围参数
   */
  params?: any;
}
