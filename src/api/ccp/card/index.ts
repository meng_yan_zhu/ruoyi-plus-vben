import { defHttp } from '@/utils/http/axios';
import { ID, IDS, commonExport } from '@/api/base';
import { CardVO, CardForm, CardQuery } from './model';

/**
 * 查询卡片管理列表
 * @param params
 * @returns
 */
export function cardList(params?: CardQuery) {
  return defHttp.get<CardVO[]>({ url: '/ccp/card/list', params });
}

/**
 * 查询卡片管理列表
 * @param params
 * @returns
 */
export function cardListAll(params?: CardQuery) {
  return defHttp.get<CardVO[]>({ url: '/ccp/card/listAll', params });
}

/**
 * 导出卡片管理列表
 * @param params
 * @returns
 */
export function cardExport(params?: CardQuery) {
  return commonExport('/ccp/card/export', params ?? {});
}

/**
 * 查询卡片管理详细
 * @param id id
 * @returns
 */
export function cardInfo(id: ID) {
  return defHttp.get<CardVO>({ url: '/ccp/card/' + id });
}

/**
 * 新增卡片管理
 * @param data
 * @returns
 */
export function cardAdd(data: CardForm) {
  return defHttp.postWithMsg<void>({ url: '/ccp/card', data });
}

/**
 * 更新卡片管理
 * @param data
 * @returns
 */
export function cardUpdate(data: CardForm) {
  return defHttp.putWithMsg<void>({ url: '/ccp/card', data });
}

/**
 * 删除卡片管理
 * @param id id
 * @returns
 */
export function cardRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/ccp/card/' + id });
}
