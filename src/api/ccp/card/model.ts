import { BaseEntity, PageQuery } from '@/api/base';

export interface CardVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 卡名称
   */
  cardName: string;

  /**
   * 卡类ID
   */
  cardClassId: string | number;

  /**
   * 类别（个人、家庭）
   */
  type: number;

  /**
   * 年限
   */
  life: number;

  /**
   * 卡号前缀
   */
  cardNoPrefix: string;

  /**
   * 卡面图片
   */
  cover: string;

  /**
   * 销售终端（激活页配置值）
   */
  channelType: string;

  /**
   * 头部背景图
   */
  headBg: string;

  /**
   * 服务申请背景图
   */
  serviceBg: string;

  /**
   * 表单字段
   */
  activateFields: string;

  /**
   * 开启弹窗 0-关 1-开
   */
  openHint: number;

  /**
   * 弹窗内容
   */
  hintMessage: string;

  /**
   * 强制激活(1不允许2允许)
   */
  forceActivate: string;

  /**
   * 生效逻辑1不执行2执行
   */
  effectiveLogic: string;

  /**
   * 生效开关1关闭2开启
   */
  effectiveSwitch: string;

  /**
   * 绑定人数限制
   */
  bindNumLimit: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus: number;

  /**
   * 家庭成员绑定期间
   */
  bindPeriod: number;

  /**
   * 是否去重
   */
  ifRepeat: string;

  /**
   * 年期规则 0-无规则 1-一年期规则
   */
  yearRule: number;

  /**
   * 激活最小年龄（月份）
   */
  activateMinAge: number;

  /**
   * 激活最大年龄（月份）
   */
  activateMaxAge: number;

  /**
   * 绑定最小年龄（月份）
   */
  bindMinAge: number;

  /**
   * 绑定最大年龄（月份）
   */
  bindMaxAge: number;

  /**
   * 有无保险公司  0否 1是
   */
  hasInsuranceCompany: number;
}

export interface CardForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 卡名称
   */
  cardName?: string;

  /**
   * 卡类ID
   */
  cardClassId?: string | number;

  /**
   * 类别（个人、家庭）
   */
  type?: number;

  /**
   * 年限
   */
  life?: number;

  /**
   * 卡号前缀
   */
  cardNoPrefix?: string;

  /**
   * 卡面图片
   */
  cover?: string;

  /**
   * 销售终端（激活页配置值）
   */
  channelType?: string;

  /**
   * 头部背景图
   */
  headBg?: string;

  /**
   * 服务申请背景图
   */
  serviceBg?: string;

  /**
   * 表单字段
   */
  activateFields?: string;

  /**
   * 开启弹窗 0-关 1-开
   */
  openHint?: number;

  /**
   * 弹窗内容
   */
  hintMessage?: string;

  /**
   * 强制激活(1不允许2允许)
   */
  forceActivate?: string;

  /**
   * 生效逻辑1不执行2执行
   */
  effectiveLogic?: string;

  /**
   * 生效开关1关闭2开启
   */
  effectiveSwitch?: string;

  /**
   * 绑定人数限制
   */
  bindNumLimit?: number;

  /**
   * 数据状态（0代表存在 1代表删除）
   */
  dataStatus?: number;

  /**
   * 家庭成员绑定期间
   */
  bindPeriod?: number;

  /**
   * 是否去重
   */
  ifRepeat?: string;

  /**
   * 年期规则 0-无规则 1-一年期规则
   */
  yearRule?: number;

  /**
   * 激活最小年龄（月份）
   */
  activateMinAge?: number;

  /**
   * 激活最大年龄（月份）
   */
  activateMaxAge?: number;

  /**
   * 绑定最小年龄（月份）
   */
  bindMinAge?: number;

  /**
   * 绑定最大年龄（月份）
   */
  bindMaxAge?: number;

  /**
   * 有无保险公司  0否 1是
   */
  hasInsuranceCompany?: number;
}

export interface CardQuery extends PageQuery {
  /**
   * 卡名称
   */
  cardName?: string;

  /**
   * 卡类ID
   */
  cardClassId?: string | number;

  /**
   * 类别（个人、家庭）
   */
  type?: number;

  /**
   * 年限
   */
  life?: number;

  /**
   * 日期范围参数
   */
  params?: any;
}
