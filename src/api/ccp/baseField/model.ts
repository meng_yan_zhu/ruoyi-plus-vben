import { BaseEntity, PageQuery } from '@/api/base';

export interface BaseFieldVO {
  /**
   * 类型 1-激活字段 2-权益字段 3-客户来源 4-服务项目 5-报价字段
   */
  type: number;

  /**
   * 字段名称
   */
  labelName: string;

  /**
   * 字段变量
   */
  variableName: string;

  /**
   * 控件类型
   */
  fieldType: string;

  /**
   * 输入提示
   */
  inputHint: string;

  /**
   * 创建时间
   */
  createTime: string;
}

export interface BaseFieldForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 类型 1-激活字段 2-权益字段 3-客户来源 4-服务项目 5-报价字段
   */
  type?: number;

  /**
   * 字段名称
   */
  labelName?: string;

  /**
   * 字段变量
   */
  variableName?: string;

  /**
   * 控件类型
   */
  fieldType?: string;

  /**
   * 输入提示
   */
  inputHint?: string;

  /**
   * 来源类型 1-字典 2-接口
   */
  fromType?: number;

  /**
   * 接口路径
   */
  apiUrl?: string;

  /**
   * 控件值来源
   */
  fieldDictValueFrom?: string;

  /**
   * 控件值
   */
  fieldValue?: string;

  /**
   * 初始值来源
   */
  initialValueFrom?: string;

  /**
   * 初始值
   */
  initialValue?: string;

  /**
   * 日期格式化类型
   */
  dateFormat?: string;

  /**
   * 级联类型 1-省 2-省市 3-省市区
   */
  cascaderType?: number;

  /**
   * 删除标志（0代表存在 1代表删除）
   */
  dataStatus?: number;

  /**
   * 提示框类型 0-无 1-文字 2-图片
   */
  tooltipType?: number;

  /**
   * 提示框内容
   */
  tooltipContent?: string;
}

export interface BaseFieldQuery extends PageQuery {
  /**
   * 类型 1-激活字段 2-权益字段 3-客户来源 4-服务项目 5-报价字段
   */
  type?: number;

  /**
   * 字段名称
   */
  labelName?: string;

  /**
   * 字段变量
   */
  variableName?: string;

  /**
   * 提示框类型 0-无 1-文字 2-图片
   */
  tooltipType?: number;

  /**
   * 提示框内容
   */
  tooltipContent?: string;

  /**
   * 日期范围参数
   */
  params?: any;
}
