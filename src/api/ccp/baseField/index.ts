import { defHttp } from '@/utils/http/axios';
import { ID, IDS, commonExport } from '@/api/base';
import { BaseFieldVO, BaseFieldForm, BaseFieldQuery } from './model';

/**
 * 查询基础字段列表
 * @param params
 * @returns
 */
export function baseFieldList(params?: BaseFieldQuery) {
  return defHttp.get<BaseFieldVO[]>({ url: '/ccp/baseField/list', params });
}

/**
 * 查询基础字段列表
 * @param params
 * @returns
 */
export function baseFieldListAll(params?: BaseFieldQuery) {
  return defHttp.get<BaseFieldVO[]>({ url: '/ccp/baseField/listAll', params });
}

/**
 * 导出基础字段列表
 * @param params
 * @returns
 */
export function baseFieldExport(params?: BaseFieldQuery) {
  return commonExport('/ccp/baseField/export', params ?? {});
}

/**
 * 查询基础字段详细
 * @param id id
 * @returns
 */
export function baseFieldInfo(id: ID) {
  return defHttp.get<BaseFieldVO>({ url: '/ccp/baseField/' + id });
}

/**
 * 新增基础字段
 * @param data
 * @returns
 */
export function baseFieldAdd(data: BaseFieldForm) {
  return defHttp.postWithMsg<void>({ url: '/ccp/baseField', data });
}

/**
 * 更新基础字段
 * @param data
 * @returns
 */
export function baseFieldUpdate(data: BaseFieldForm) {
  return defHttp.putWithMsg<void>({ url: '/ccp/baseField', data });
}

/**
 * 删除基础字段
 * @param id id
 * @returns
 */
export function baseFieldRemove(id: ID | IDS) {
  return defHttp.deleteWithMsg<void>({ url: '/ccp/baseField/' + id });
}
